#!groovy
 
pipeline {
  agent {
    label 'host'
  }
  
  options {
    buildDiscarder(logRotator(numToKeepStr: '10', daysToKeepStr: '7'))
    disableConcurrentBuilds()
  }
  
  stages {
 
    stage("Set Build Parameters") {
      steps {
        script {
          currentBuild.displayName = "Build_App .${BUILD_NUMBER}";
        }
      }
    }

    stage("Build") {
      steps {
        dir("./app"){
          sh 'docker build -t helloworldnodejs .'
        }
      }
    }

    stage("Code Quality") {
      steps {
        dir("./app") {
          withSonarQubeEnv('Sonarqube') {
            withCredentials([usernamePassword(credentialsId: 'adoplight', passwordVariable: 'rootPassword', usernameVariable: 'rootUser')]) {
              sh "sudo dos2unix /bin/bash /var/adoplight/sonarqube/bin/sonar-scanner"
              sh "/bin/bash /var/adoplight/sonarqube/bin/sonar-scanner -D sonar.user=${rootUser} -D sonar.projectKey=helloworldnodejs -D sonar.sources=."
            }
          }
          timeout(time: 1, unit: 'HOURS') {
            waitForQualityGate abortPipeline: true
          }

        }
      }
    }
          
    stage("Unit Test") {
      steps {
        dir("./app") {
          script {
            try {
              sh "docker run -d --name helloworldnodejs-unittest helloworldnodejs"
              sh "docker exec helloworldnodejs-unittest npm test"
            }
            finally {
              sh "docker rm -f helloworldnodejs-unittest"
            }
          }
        }
      }
    }
 
    stage("Deploy App") {
      steps {
        script {
          try {
            sh "docker rm -f helloworldnodejs"
          }
          catch(err) {
            echo "no running instance."
          }
          finally {
            sh "docker run -d -p 9100:80 --name helloworldnodejs helloworldnodejs"
          }
        }
      }
    }
 
    stage("Integration Test") {
      steps {
        script {
          try{
            dir("./app"){
              sh 'mkdir -p .results'
              sleep 5
              sh "docker run --dns ${IPADDRESS} --rm -v $WORKSPACE/.results:/work/.results helloworldnodejs npm run-script itest"
            }
          }
          finally{
            archiveArtifacts  '.results/*.png'
            archiveArtifacts  '.results/reports/*.json'
          }
        }
      }
    }
  
  }
 
  post { 
    always { 
      script {
        currentBuild.description = "goto <a href=http://${PUBLIC_IPADDRESS}:9100>App</a>"
        try {
          sh "docker rm -f helloworldnodejs-unittest"
        }
        catch(err) {
          echo "No running Container"
        }
      }
    }
  }

}
