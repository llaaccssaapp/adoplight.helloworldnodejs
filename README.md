# Hello World NodeJS Plugin

## How to setup
1. Startup ADOPlight
2. Trigger the Build **ADOPlight/Load Plugin** with the following properties:
    - ProjectName: YourName
    - PluginUrl: https://bitbucket.org/lauener/ata.git 
    - Branch: */master
    - CopyRepository: true 
3. Trigger the Build **YourName/Create_Environment**
